package edu.northeastern.plagarismdetector.serviceTests;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.fail;

import edu.northeastern.plagarismdetector.util.FileHandler;

/**
 * FileHandlerTest class tests the FileHandler class.
 */
public class FileHandlerTest {

  @Test
  public void testInvalidPath1() {
    FileHandler handler = new FileHandler();

    try {
      handler.fileWalk("0");
      fail("An exception should have been thrown");
    } catch (IOException e){
      Assert.assertEquals("Invalid File Path!", e.getMessage());
    }
  }

  @Test
  public void testInvalidPath2() {
    FileHandler handler = new FileHandler();

    try {
      handler.fileWalk("");
      fail("An exception should have been thrown");
    } catch (IOException e){
      Assert.assertEquals("Invalid File Path!", e.getMessage());
    }
  }

  @Test
  public void testInvalidPath3() {
    FileHandler handler = new FileHandler();

    try {
      handler.fileWalk(null);
      fail("An exception should have been thrown");
    } catch (IOException e){
      Assert.assertEquals("Invalid File Path!", e.getMessage());
    }
  }

  //TODO Tests for setInDirectory using project path and files

  //TODO Tests for getFileList using project path and files

  //TODO Tests for getDirectoryList using project path and files

  //TODO Tests for getDirectoryList using project path and files

  //TODO Tests for getNumFiles using project path and files

  //TODO Tests for getNumDirectories using project path and files

}