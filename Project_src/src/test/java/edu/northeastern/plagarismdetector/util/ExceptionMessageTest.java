package edu.northeastern.plagarismdetector.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * ExceptionMessageTest class tests the ExceptionMessage Enums.
 */
public class ExceptionMessageTest {

  @Test
  public void testINVALID_FILE_PATH(){
    ExceptionMessage message = ExceptionMessage.INVALID_FILE_PATH;
    String exp = "Invalid File Path!";

    assertEquals(exp,message.getMsg());
  }
}