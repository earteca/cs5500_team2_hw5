package edu.northeastern.plagarismdetector.dao;

import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;
import java.util.ArrayList;
import java.util.List;

import edu.northeastern.plagarismdetector.model.AbstractUser;
import edu.northeastern.plagarismdetector.model.IUser;
import edu.northeastern.plagarismdetector.model.Student;

/**
 * adapted from https://www.freecodecamp.org/news/spring-boot-tutorial/
 */
@Repository("studentDao")
public class StudentRESTDataAccessService implements IUserDAO {

  private static List<IUser> DB = new ArrayList<>();

  @Override
  public int insertUser(UUID id, AbstractUser student) {
    //TODO remove instance of
    if (!(student instanceof Student)) {
      return 0;


      //TODO Fail if unique student already in DB
      //      if (already in DB)
      //      return 0;
    } else {
      DB.add(new Student(id, student.getFirstName(), student.getLastName(), student.getEmail()));
      return 1;
    }
  }

  @Override
  public List<IUser> selectAllUsers() {
    return DB;
  }

  @Override
  public Optional<IUser> selectUserById(UUID id) {
    return DB.stream()
            .filter(student -> student.getId().equals(id))
            .findFirst();
  }

  @Override
  public int deleteUserById(UUID id) {
    Optional<IUser> studentMaybe = selectUserById(id);
    if (!studentMaybe.isPresent()){
      return 0;
    }
    DB.remove(studentMaybe.get());
    return 1;
  }

  @Override
  public int updateUserById(UUID id, IUser update) {
    return selectUserById(id)
            .map(student -> {
              int idxOfStudentToUpdate = DB.indexOf(student);
              if (idxOfStudentToUpdate >= 0) {
                DB.set(idxOfStudentToUpdate, new Student(id, update.getFirstName(), update.getLastName(), update.getEmail()));
                return 1;
              }
              return 0;
            })
            .orElse(0);
  }
}