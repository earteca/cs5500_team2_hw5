package edu.northeastern.plagarismdetector.dao;

import org.springframework.data.repository.CrudRepository;

import edu.northeastern.plagarismdetector.entity.InstructorEntity;
import edu.northeastern.plagarismdetector.entity.StudentEntity;


/**
 * adapted from https://spring.io/guides/gs/accessing-data-mysql/
 */
// This will be AUTO IMPLEMENTED by Spring into a Bean called studentRepository
// CRUD refers Create, Read, Update, Delete

public interface StudentRepository extends CrudRepository<StudentEntity, Integer> {

}