package edu.northeastern.plagarismdetector.dao;

import edu.northeastern.plagarismdetector.model.AbstractUser;
import edu.northeastern.plagarismdetector.model.IUser;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * adapted from https://www.freecodecamp.org/news/spring-boot-tutorial/
 */
public interface IUserDAO {

  int insertUser(UUID id, AbstractUser user);

  default int addUser(AbstractUser student) {
    UUID id = UUID.randomUUID();
    return insertUser(id, student);
  }

  List<IUser> selectAllUsers();

  Optional<IUser> selectUserById(UUID id);

  int deleteUserById(UUID id);

  int updateUserById(UUID id, IUser user);
}
