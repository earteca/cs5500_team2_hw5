package edu.northeastern.plagarismdetector.dao;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import edu.northeastern.plagarismdetector.model.AbstractUser;
import edu.northeastern.plagarismdetector.model.IUser;
import edu.northeastern.plagarismdetector.model.Instructor;

/**
 * adapted from https://www.freecodecamp.org/news/spring-boot-tutorial/
 */
@Repository("instructorDao")
public class InstructorRESTDataAccessService implements IUserDAO {
  protected static List<IUser> DB = new ArrayList<>();


  @Override
  public int insertUser(UUID id, AbstractUser instructor) {
    //TODO remove instance of
    if (!(instructor instanceof Instructor)) {
      return 0;


    //TODO Fail if unique instructor already in DB
    //      if (already in DB)
    //      return 0;
    } else {
      DB.add(new Instructor(id, instructor.getFirstName(), instructor.getLastName(), instructor.getEmail()));
      return 1;
    }
  }

  @Override
  public List<IUser> selectAllUsers() {
    return DB;
  }

  @Override
  public Optional<IUser> selectUserById(UUID id) {
    return DB.stream()
            .filter(instructor -> instructor.getId().equals(id))
            .findFirst();
  }

  @Override
  public int deleteUserById(UUID id) {
    Optional<IUser> instructorMaybe = selectUserById(id);
    if (!instructorMaybe.isPresent()){
      return 0;
    }
    DB.remove(instructorMaybe.get());
    return 1;
  }

  @Override
  public int updateUserById(UUID id, IUser update) {
    return selectUserById(id)
            .map(instructor -> {
              int idxOfInstructorToUpdate = DB.indexOf(instructor);
              if (idxOfInstructorToUpdate >= 0) {
                DB.set(idxOfInstructorToUpdate, new Instructor(id, update.getFirstName(), update.getLastName(), update.getEmail()));
                return 1;
              }
              return 0;
            })
            .orElse(0);
  }
}
