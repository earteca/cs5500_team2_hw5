package edu.northeastern.plagarismdetector.model;

import java.util.UUID;

import javax.mail.internet.InternetAddress;

/**
 * Class Student represents a student user of the software.
 */
public class Student extends AbstractUser{

  public Student(UUID id, String first, String last, InternetAddress email) {
    super(id, first, last, email);
  }
}