package edu.northeastern.plagarismdetector.model;

import java.util.UUID;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.northeastern.plagarismdetector.util.ExceptionMessage;

/**
 * adapted from https://www.freecodecamp.org/news/spring-boot-tutorial/
 */

/**
 * Abstract class AbstractUser represents the shared fields of an IUser.
 */
public abstract class AbstractUser implements IUser{

  private final UUID id;
  @NotBlank
  private final String firstName;
  @NotBlank
  private final String lastName;
  @NotBlank
  private final InternetAddress email;

  /**
   * AbstractUser constructor method. The constructor validates the user's first name, last name,
   * and email address before storing.
   *
   * @param id a UUID (Universally Unique Identifier) number for the user.
   * @param first a String representing the user's first name.
   * @param last a String representing the user's last name.
   * @param email a String representing the user's email address.
   *
   * @throws AddressException if the user's first name contains a non-alphabetic character.
   * @throws AddressException if the user's last name contains a non-alphabetic character.
   * @throws AddressException if the user's email is an invalid RFC822 internet address.
   * @throws AddressException if the user's email is from an unsupported hostname.
   */
  public AbstractUser(@JsonProperty("id") UUID id,
                      @JsonProperty("first") String first,
                      @JsonProperty("last") String last,
                      @JsonProperty("email") InternetAddress email) {
    this.id = id;
    this.firstName = this.validateName(first);
    this.lastName = this.validateName(last);
//    this.email = this.validateEmail(email);
    this.email = email;
  }


  public UUID getId() {
    return this.id;
  }


  public String getFullName() {
    return this.lastName + ", " + this.firstName;
  }


  public String getFirstName(){ return this.firstName; }


  public String getLastName() {return this.lastName; }


  public InternetAddress getEmail() {
    return this.email;
  }

  //TODO this may need to be moved to controller
  /**
   * validateName method validates whether the provided name contains a non-alphabetic character.
   * The same method can be used to validate first and last names. All names are converted to
   * upper-case.
   *
   * @param name a String representing a user's name, either first or last.
   * @throws IllegalArgumentException "Improperly Formatted Name. Name May Only Contain Alphabetical
   *         Characters!"
   * @return a String representing validated name converted to upper-case.
   */
  private String validateName(String name) throws IllegalArgumentException{
    if (!name.matches("[A-Za-z]")){
      throw new IllegalArgumentException(ExceptionMessage.INVALID_NAME.getMsg());
    }
    return name.toUpperCase();
  }

  //TODO this may need to be moved to controller
  /**
   * validateEmail method validates whether the user's provided email address is of supported RFC822
   * Internet email address syntax. The method also validates whether the user's email is from a
   * supported hostname. Currently supported hostnames include: "husky.neu.edu", "northeastern.edu".
   *
   * @param email a String representing a user's email address.
   * @throws AddressException if the user's email is an invalid RFC822 internet address.
   * @throws AddressException if the user's email is from an unsupported hostname.
   *
   * @return a InternetAddress representation of the user's email address.
   */
  private InternetAddress validateEmail(String email) throws AddressException {
    InternetAddress address = new InternetAddress(email);

    try {
      address.validate();
    } catch (AddressException e) {
      throw new AddressException(ExceptionMessage.INVALID_EMAIL_ADDR_FORMAT.getMsg());
    }

    String[] addr = email.split("[@]");
    if (!(addr[1].equals("husky.neu.edu") || addr[1].equals("northeastern.edu"))){
      throw new AddressException(ExceptionMessage.UNSUPPORTED_EMAIL_ADDR_HOST.getMsg());
    }
    return address;
  }
}