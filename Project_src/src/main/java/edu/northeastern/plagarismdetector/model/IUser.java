package edu.northeastern.plagarismdetector.model;

import java.util.UUID;
import javax.mail.internet.InternetAddress;

/**
 * Interface IUser represents a user of the software. At this time supported IUsers are either
 * students or instructors.
 */
public interface IUser {

  /**
   * getId method obtains the unique identifier number for an IUser.
   *
   * @return a UUID (Universally Unique Identifier) number
   */
  public UUID getId();

  /**
   * getFullName method obtains the name for an IUser. All first and last names are in upper-case.
   *
   * @return a String representing the name in the following format: "LASTNAME, FIRST".
   */
  public String getFullName();

  /**
   * getFirstName method obtains the first name for an IUser. First name is stored in upper-case.
   *
   * @return a String representing the first name in the following format: "FIRST".
   */
  public String getFirstName();

  /**
   * getLastName method obtains the last name for an IUser. Last name is stored in upper-case.
   *
   * @return a String representing the last name in the following format: "LAST".
   */
  public String getLastName();

  /**
   * getEmail method obtains the email address for an IUser.
   *
   * @return a InternetAddress email representation according to RFC822 syntax.
   */
  public InternetAddress getEmail();
}