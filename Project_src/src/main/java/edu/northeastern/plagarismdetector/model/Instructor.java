package edu.northeastern.plagarismdetector.model;

import java.util.UUID;

import javax.mail.internet.InternetAddress;

//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Class Instructor represents an instructor user of the software.
 */
public class Instructor extends AbstractUser {

  private String hashedPassword;

  public Instructor(UUID id, String first, String last, InternetAddress email)  {
    super(id, first, last, email);
  }

  //TODO: Hash Password

  //TODO: Get Password
}