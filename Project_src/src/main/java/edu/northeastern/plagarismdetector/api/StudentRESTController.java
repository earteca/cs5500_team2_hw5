package edu.northeastern.plagarismdetector.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import edu.northeastern.plagarismdetector.model.IUser;
import edu.northeastern.plagarismdetector.model.Student;
import edu.northeastern.plagarismdetector.service.StudentService;

/**
 * adapted from https://www.freecodecamp.org/news/spring-boot-tutorial/
 */
@RequestMapping("api/v1/student")
@RestController
public class StudentRESTController {

  private final StudentService studentService;

  @Autowired
  public StudentRESTController(StudentService studentService){
    this.studentService = studentService;
  }

  @PostMapping
  public void addStudent(@Valid
                         @NotNull
                         @RequestBody Student student) {
    this.studentService.addStudent(student);
  }

  @GetMapping
  public List<IUser> getAllStudents(){
    return this.studentService.getAllStudents();
  }

  @GetMapping(path = "{id}")
  public IUser getStudentById(@PathVariable("id") UUID id) {
    //  TODO: replace nullwith custom exception
    return this.studentService.getStudentById(id)
            .orElse(null);
  }

  @DeleteMapping(path = "{id}")
  public void deleteStudentById(@PathVariable("id") UUID id) {
    this.studentService.deleteStudent(id);
  }

  @PutMapping(path = "{id}")
  public void updateStudent(@PathVariable("id") UUID id,
                                 @Valid
                                 @NotNull
                                 @RequestBody IUser studentToUpdate) {
    this.studentService.updateStudent(id, studentToUpdate);
  }
}
