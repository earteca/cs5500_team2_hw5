package edu.northeastern.plagarismdetector.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import edu.northeastern.plagarismdetector.model.IUser;
import edu.northeastern.plagarismdetector.model.Instructor;
import edu.northeastern.plagarismdetector.service.InstructorService;

/**
 * adapted from https://www.freecodecamp.org/news/spring-boot-tutorial/
 */
@RequestMapping("api/v1/instructor")
@RestController
public class InstructorRESTController {

  private final InstructorService instructorService;

  @Autowired
  public InstructorRESTController(InstructorService instructorService){
    this.instructorService = instructorService;
  }

  @PostMapping
  public void addInstructor(@Valid
                            @NotNull
                            @RequestBody Instructor instructor) {
    this.instructorService.addInstructor(instructor);
  }

  @GetMapping
  public List<IUser> getAllInstructors(){
    return this.instructorService.getAllInstructors();
  }

  @GetMapping(path = "{id}")
  public IUser getInstructorById(@PathVariable("id") UUID id) {
    //  TODO: replace null with custom exception
    return this.instructorService.getInstructorById(id)
            .orElse(null);
  }

  @DeleteMapping(path = "{id}")
  public void deleteInstructorById(@PathVariable("id") UUID id) {
    this.instructorService.deleteInstructor(id);
  }

  @PutMapping(path = "{id}")
  public void updateInstructor(@PathVariable("id") UUID id,
                               @Valid
                               @NotNull
                               @RequestBody IUser instructorToUpdate) {
    this.instructorService.updateInstructor(id, instructorToUpdate);
  }
}
