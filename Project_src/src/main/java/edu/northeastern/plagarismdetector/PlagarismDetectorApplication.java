package edu.northeastern.plagarismdetector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlagarismDetectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlagarismDetectorApplication.class, args);
	}
}