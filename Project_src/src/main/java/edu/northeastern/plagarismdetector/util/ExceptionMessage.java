package edu.northeastern.plagarismdetector.util;

/**
 * Enum class ExceptionMessage defines in a central location different error messages for exception
 * handling that would otherwise be stored as strings in multiple individual methods.
 */
public enum ExceptionMessage {

  INVALID_FILE_PATH("Invalid File Path!"),
  INVALID_EMAIL_ADDR_FORMAT("Improperly Formatted Email Address!"),
  UNSUPPORTED_EMAIL_ADDR_HOST("Unsupported Email Address Host. Address must have either a " +
          "@husky.neu.edu or @northeastern.edu hostname."),
  INVALID_NAME("Improperly Formatted Name. Name May Only Contain Alphabetical Characters!");


  private final String msg;

  /**
   * Constructor method initializes and stores this enum's string representation.
   *
   * @param message a string representing the error message of this exception.
   */
  ExceptionMessage(String message){
    this.msg = message;
  }

  /**
   * getMsg returns the string representation of this exception's error message.
   *
   * @return a String error message.
   */
  public String getMsg(){
    return this.msg;
  }
}