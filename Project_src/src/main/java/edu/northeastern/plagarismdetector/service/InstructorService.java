package edu.northeastern.plagarismdetector.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import edu.northeastern.plagarismdetector.dao.IUserDAO;
import edu.northeastern.plagarismdetector.model.IUser;
import edu.northeastern.plagarismdetector.model.Instructor;

/**
 * adapted from https://www.freecodecamp.org/news/spring-boot-tutorial/
 */

@Service
public class InstructorService {

  private final IUserDAO userDAO;

  @Autowired
  public InstructorService(@Qualifier("instructorDao") IUserDAO userDAO) {
    this.userDAO = userDAO;
  }

  public int addInstructor(Instructor instructor){
    return this.userDAO.addUser(instructor);
  }

  public List<IUser> getAllInstructors(){
    return this.userDAO.selectAllUsers();
  }

  public Optional<IUser> getInstructorById(UUID id){
    return this.userDAO.selectUserById(id);
  }

  public int deleteInstructor(UUID id) {
    return this.userDAO.deleteUserById(id);
  }

  public int updateInstructor(UUID id, IUser newInstructor){
    return this.userDAO.updateUserById(id, newInstructor);
  }
}
