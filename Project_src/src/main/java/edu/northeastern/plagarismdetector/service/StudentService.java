package edu.northeastern.plagarismdetector.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import edu.northeastern.plagarismdetector.dao.IUserDAO;
import edu.northeastern.plagarismdetector.model.IUser;
import edu.northeastern.plagarismdetector.model.Student;

/**
 * adapted from https://www.freecodecamp.org/news/spring-boot-tutorial/
 */

@Service
public class StudentService {

  private final IUserDAO userDAO;

  @Autowired
  public StudentService(@Qualifier("studentDao") IUserDAO userDAO) {
    this.userDAO = userDAO;
  }

  public int addStudent(Student student){
    return this.userDAO.addUser(student);
  }

  public List<IUser> getAllStudents(){
    return this.userDAO.selectAllUsers();
  }

  public Optional<IUser> getStudentById(UUID id){
    return this.userDAO.selectUserById(id);
  }

  public int deleteStudent(UUID id) {
    return this.userDAO.deleteUserById(id);
  }

  public int updateStudent(UUID id, IUser newStudent){
    return this.userDAO.updateUserById(id, newStudent);
  }

}
